# Bash comon functions

## Initialization

Add the following line(s) to your script

```bash
# Script initialization
APP=$(readlink -e $0) && . "$(dirname $APP)/function.d/init" || (echo "Script initialization error" >&2; exit 1)
```

Common variables

- PID, an application PID
- CWD, current working directory
- BASEDIR, application base/origin directory
- BASENAME, application origin name
- APPDIR, application directory
- APPNAME, application name

## Common function

Include another script file

```bash
include_file "some-script"
```

Initialize application environment

```bash
attach_env
```

## Lock function

### Module initialization

```bash
# Include "lock" module
include_file "lock"
```

Lock application

```bash
# lock the app or exit if the app is running
LOCK_FILE="$HOME/${APPNAME}.lock"
lock_or_exit
```
